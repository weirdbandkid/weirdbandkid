# Hi, I'm *weirdbandkid*
I am a self-taught programmer in JavaScript. I wouldn't call myself good programmer any kind. If the code works it works. Am I right?

Here are my GitHub Statistics

![github statistics](https://github-readme-stats.vercel.app/api?username=weirdbandkid&show_icons=true&theme=tokyonight)

<a href="https://github.com/weirdbandkid">
  <img align="center" src="https://github-readme-stats.anuraghazra1.vercel.app/api/top-langs/?username=weirdbandkid&layout=compact&theme=material-palenight" />
</a>

<a href="https://github.com/weirdbandkid">
  <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=weirdbandkid&repo=mod-bot&theme=material-palenight" />
</a>    
<a href="https://github.com/weirdbandkid/weirdbandkid.github.io">
  <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=weirdbandkid&repo=weirdbandkid.github.io&theme=material-palenight" />
</a>

![](https://komarev.com/ghpvc/?username=weirdbandkid)
